# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html
from util import Stack, Queue, PriorityQueue
from game import Directions
"""
In search.py, you will implement generic search algorithms which are called
by Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples,
        (successor, action, stepCost), where 'successor' is a
        successor to the current state, 'action' is the action
        required to get there, and 'stepCost' is the incremental
        cost of expanding to that successor
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.  The sequence must
        be composed of legal moves
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other
    maze, the sequence of moves will be incorrect, so only use this for tinyMaze
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s,s,w,s,w,w,s,w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first

    Your search algorithm needs to return a list of actions that reaches
    the goal.  Make sure to implement a graph search algorithm

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***" 
    if problem.isGoalState(problem.getStartState()):
        return [Directions.STOP]
    succ = Stack()
    realPath = []
    v = set()
    prevAct = {}
    prevSt = {}
    v.add(problem.getStartState())
    for s in problem.getSuccessors(problem.getStartState()):
        prevSt[s[0]]=problem.getStartState()
        prevAct[s[0]] = s[1]
        succ.push(s)
    s=None
    while not succ.isEmpty():
        s = succ.pop()
        if(problem.isGoalState(s[0])):
            break
        v.add(s[0])
        for ss in problem.getSuccessors(s[0]):
            if ss[0] not in v:
                v.add(ss[0])
                succ.push(ss)
                prevAct[ss[0]] = ss[1]
                prevSt[ss[0]] = s[0]
    s=s[0]
    while not s==problem.getStartState():
        realPath=[prevAct[s]]+realPath
        s=prevSt[s]
    return realPath

def breadthFirstSearch(problem):
    """
    Search the shallowest nodes in the search tree first.
    """
    "*** YOUR CODE HERE ***"
    if problem.isGoalState(problem.getStartState()):
        return [Directions.STOP]
    succ = Queue()
    realPath = []
    v = set()
    prevAct = {}
    prevSt = {}
    v.add(problem.getStartState())
    for s in problem.getSuccessors(problem.getStartState()):
        prevSt[s[0]]=problem.getStartState()
        prevAct[s[0]] = s[1]
        succ.push(s)
    s=None
    while not succ.isEmpty():
        s = succ.pop()
        if(problem.isGoalState(s[0])):
            break
        v.add(s[0])
        for ss in problem.getSuccessors(s[0]):
            if ss[0] not in v:
                v.add(ss[0])
                succ.push(ss)
                prevAct[ss[0]] = ss[1]
                prevSt[ss[0]] = s[0]
    s=s[0]
    while not s==problem.getStartState():
        realPath=[prevAct[s]]+realPath
        s=prevSt[s]
    return realPath

def uniformCostSearch(problem):
    "Search the node of least total cost first. "
    "*** YOUR CODE HERE ***"
    if problem.isGoalState(problem.getStartState()):
        return [Directions.STOP]
    succ = PriorityQueue()
    realPath = []
    v = set()
    prevAct = {}
    prevSt = {}
    v.add(problem.getStartState())
    for s in problem.getSuccessors(problem.getStartState()):
        prevSt[s[0]]=problem.getStartState()
        prevAct[s[0]] = s[1]
        succ.push(s,problem.getCostOfActions([s[1]]))
    s=None
    while not succ.isEmpty():
        s = succ.pop()
        if(problem.isGoalState(s[0])):
            break
        v.add(s[0])
        for ss in problem.getSuccessors(s[0]):
            if ss[0] not in v:
                prevAct[ss[0]] = ss[1]
                prevSt[ss[0]] = s[0]
                ts=ss[0]
                tPath=[]
                while not ts==problem.getStartState():
                    tPath=[prevAct[ts]]+tPath
                    ts=prevSt[ts]
                succ.push(ss,problem.getCostOfActions(tPath))           
    s=s[0]
    while not s==problem.getStartState():
        realPath=[prevAct[s]]+realPath
        s=prevSt[s]
    return realPath

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    "Search the node that has the lowest combined cost and heuristic first."
    "*** YOUR CODE HERE ***"
    if problem.isGoalState(problem.getStartState()):
        return [Directions.STOP]
    succ = PriorityQueue()
    realPath = []
    v = set()
    prevAct = {}
    prevSt = {}
    gScore={}
    v.add(problem.getStartState())
    for s in problem.getSuccessors(problem.getStartState()):
        prevSt[s[0]]=problem.getStartState()
        prevAct[s[0]] = s[1]
        gScore[s[0]] = problem.getCostOfActions([s[1]])
        succ.push(s,gScore[s[0]]+heuristic(s[0],problem))
    s=None
    while not succ.isEmpty():
        s = succ.pop()
        if(problem.isGoalState(s[0])):
            break
        v.add(s[0])
        for ss in problem.getSuccessors(s[0]):
            if ss[0] not in v:
                tgs = gScore[s[0]]+problem.getCostOfActions([ss[1]])
                if ss[0] not in gScore or gScore[ss[0]]>tgs:
                    prevAct[ss[0]] = ss[1]
                    prevSt[ss[0]] = s[0]
                    gScore[ss[0]] = tgs
                    succ.push(ss,gScore[ss[0]]+heuristic(ss[0],problem))
    s=s[0]
    while not s==problem.getStartState():
        realPath=[prevAct[s]]+realPath
        s=prevSt[s]
    return realPath


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
